#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <protobuf>
#include <cstrike>

//#pragma newdecls required

public Plugin:myinfo =
{
	name = "Items RPG",
	author = "XzaR",
	description = "A item finding mod for CS:GO based on RPG games.",
	version = "1.0",
	url = "http://www.xuniver.se/"
};

#define ABILITY_COOLDOWN 		1.0

ConVar CVEnabled;
ConVar CVUrl;

ConVar CVDBDriver;
ConVar CVDBName;
ConVar CVDBUsername;
ConVar CVDBPassword;
ConVar CVDBHostname;

#include "itemsrpg/headers/sprite.sp"
#include "itemsrpg/headers/sound.sp"
#include "itemsrpg/headers/player.sp"
#include "itemsrpg/headers/item.sp"

#include "itemsrpg/utilities.sp"
#include "itemsrpg/dataqueries.sp"
#include "itemsrpg/data.sp"
#include "itemsrpg/effects.sp"
#include "itemsrpg/game.sp"
#include "itemsrpg/client.sp"
#include "itemsrpg/hooks.sp"
#include "itemsrpg/events.sp"
#include "itemsrpg/menus.sp"

public OnPluginStart()
{
	CVEnabled 	= CreateConVar("itemsrpg_enabled", "1", "Sets whether Items RPG is enabled.");
	
	new String:defaultURL[256];
	new String:ip[16];
	Utility_GetHostIp(ip,15);
	Format(defaultURL, sizeof(defaultURL), "%s%s", "http://", ip);
	CVUrl 		= CreateConVar("itemsrpg_url", defaultURL, "Sets endpoint url for Items RPG.");
	
	CVDBDriver 		= CreateConVar("itemsrpg_db_driver" 	,"sqlite" 	,"The type of SQL driver to use.");
	CVDBName 		= CreateConVar("itemsrpg_db_name"		,"itemsrpg"	,"The name of the database to use.");
	CVDBUsername 	= CreateConVar("itemsrpg_db_username"	,"root"		,"The username to access the database.");
	CVDBPassword 	= CreateConVar("itemsrpg_db_password"	,"password"	,"The password to access the database.");
	CVDBHostname 	= CreateConVar("itemsrpg_db_hostname"	,"localhost","Hostname, the IP or domain name to connect to the SQL server.");
	
	AutoExecConfig();
	
	AddCommandListener(OnCommandSay, "say");
	AddCommandListener(OnCommandSay, "say2");
	AddCommandListener(OnCommandSay, "say_team");
	
	RegConsoleCmd("itemsrpg_menu", Menu_CommandItemsRPG);

	HookEvent("player_spawned", 	Event_PlayerSpawned,	EventHookMode_Post);
	HookEvent("player_spawn", 		Event_PlayerSpawn,		EventHookMode_Post);
	HookEvent("player_death", 		Event_PlayerDeath,		EventHookMode_Pre);
	HookEvent("player_hurt", 		Event_PlayerHurt,		EventHookMode_Pre);
	HookEvent("player_blind", 		Event_PlayerBlind,		EventHookMode_Pre);
	HookEvent("player_footstep", 	Event_PlayerFootstep,	EventHookMode_Pre);
	
	//HookEvent("player_jump", 	Event_PlayerJump,	EventHookMode_Pre);
	//HookEvent("player_falldamage", 	Event_PlayerFalldamage,	EventHookMode_Pre);
	//HookEvent("player_footstep", 	Event_PlayerFootstep,	EventHookMode_Pre);

	//HookEvent("weapon_fire", 	Event_WeaponFire,	EventHookMode_Pre);
	//HookEvent("weapon_reload", 	Event_WeaponReload,	EventHookMode_Pre);
}



public Action:OnCommandSay(client, const String:sCommand[], argc)
{
	decl String:sText[192];
	if (GetCmdArgString(sText, sizeof(sText)) < 1)
	{
		return Plugin_Continue;
	}
	
	new iStartIndex = 0;
	if (sText[strlen(sText)-1] == '"')
	{
		sText[strlen(sText)-1] = '\0';
		iStartIndex = 1;
	}
	
	if (strcmp(sCommand, "say2", false) == 0)
	{
		iStartIndex += 4;
	}
	
	if (strcmp(sText[iStartIndex], "irpg-reg", false) == 0)
	{
		PrintToChat(client, "register test.");
		return Plugin_Handled;
	}
	
	if (strcmp(sText[iStartIndex], "irpg-menu", false) == 0)
	{
		Menu_CommandItemsRPG(client,argc);
		return Plugin_Handled;
	}
	
	if (strcmp(sText[iStartIndex], "irpg", false) == 0)
	{
		new String:bufferURL[256];
		CVUrl.GetString(bufferURL, sizeof(bufferURL));
		
		Utility_MotdTypeUrl(client,"Items RPG","http://www.google.com");
		return Plugin_Handled;
	}
	
	
	return Plugin_Continue;
}

public OnMapStart()
{
	GSprites[Glow] 			= PrecacheModel("materials/sprites/Glow.vmt",true);
	GSprites[PurpleGlow] 	= PrecacheModel("materials/sprites/purpleglow1.vmt",true);
	GSprites[BlueGlow] 		= PrecacheModel("materials/sprites/blueglow2.vmt",true);
	
	PrecacheSound(IRPG_SOUND_FREEZE,true);
	
	Data_Init();
}
