public OnClientDisconnect(client)
{
	if(IsClientInGame(client))
	{
		SDKUnhook(client, SDKHook_WeaponEquipPost, Hook_WeaponEquipPost);
		SDKUnhook(client, SDKHook_OnTakeDamage, Hook_OnTakeDamage);
	}
}

public OnClientAuthorized(client)
{
	if(!IsFakeClient(client) && IsClientAuthorized(client))
	{
		decl String:sAuth[21];
		decl String:sName[65];
		
		GetClientName(client,sName,sizeof(sName));
		GetClientAuthId(client,AuthId_SteamID64,sAuth,sizeof(sAuth));
		
		Data_InsertPlayer(sAuth,sName);
		
		PrintToServer("%s NAME: %s",sAuth,sName);
	}
}

public OnClientConnected(client)
{

}

public OnClientPutInServer(client)
{
	SDKHook(client, SDKHook_WeaponEquipPost, 	Hook_WeaponEquipPost);
	SDKHook(client, SDKHook_OnTakeDamage,		Hook_OnTakeDamage);
}