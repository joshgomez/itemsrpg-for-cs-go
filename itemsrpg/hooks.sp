
public Action:Hook_WeaponEquipPost(client, weapon)
{
	if(CVEnabled.IntValue != 1)
	{
		return Plugin_Continue;
	}

	return Plugin_Continue;
}

public Action:Hook_OnTakeDamage(victim, &attacker, &inflictor, &Float:damage, &damagetype)
{
	if(CVEnabled.IntValue != 1)
	{
		return Plugin_Continue;
	}

	if(victim == attacker && !Utility_IsPlayer(victim))
	{
		return Plugin_Continue;
	}
	
	if(!IsClientInGame(victim) || !IsPlayerAlive(victim) || !IsPlayerAlive(attacker))
	{
		return Plugin_Continue;
	}

	damage += Game_ExtraDamageOnAttack(victim,attacker);
	return Plugin_Changed;
}