
static String:SDataError[255];
static Handle:HData 			= INVALID_HANDLE;
static Handle:HDataStatement 	= INVALID_HANDLE;
static Handle:HDataQuery 		= INVALID_HANDLE;

stock void DataQuery_PrintError()
{
	PrintToServer("[ItemsRPG->DataQuery]: %s", SDataError);
}

stock bool:DataQuery_OpenConnection()
{
	if(SQL_CheckConfig("itemsrpg"))
	{
		HData = SQL_Connect("itemsrpg",false,SDataError,sizeof(SDataError));
	}
	else
	{
		new String:bufferDriver[64],String:bufferDatabase[128],String:bufferUsername[128],String:bufferPassword[128],String:bufferHostname[128];
		
		CVDBDriver.GetString(bufferDriver, sizeof(bufferDriver));
		CVDBName.GetString(bufferDatabase, sizeof(bufferDatabase));
		CVDBUsername.GetString(bufferUsername, sizeof(bufferUsername));
		CVDBPassword.GetString(bufferPassword, sizeof(bufferPassword));
		CVDBHostname.GetString(bufferHostname, sizeof(bufferHostname));
	
		new Handle:hKv = CreateKeyValues("");
	
		KvSetString(hKv, "driver", bufferDriver);
		KvSetString(hKv, "database", bufferDatabase);
		KvSetString(hKv, "user", bufferUsername);
		KvSetString(hKv, "pass", bufferPassword);
		KvSetString(hKv, "host", bufferHostname);
		
		//KvSetString(hKv, "timeout", "");
		//KvSetString(hKv, "port", "");
		
		HData = SQL_ConnectCustom(hKv, SDataError, sizeof(SDataError), false);
	}
	
	if (HData == INVALID_HANDLE)
	{
		DataQuery_PrintError();
		return false;
	}
	
	return true;
}

stock bool:DataQuery_CloseConnection()
{
	if (HData != INVALID_HANDLE)
	{
		CloseHandle(HData);
		return true;
	}
	
	return false;
}

stock bool:DataQuery_Fast(const String:sQuery[])
{
	if (HData == INVALID_HANDLE)
	{
		return false;
	}

	if (!SQL_FastQuery(HData, sQuery))
	{
		SQL_GetError(HData, SDataError, sizeof(SDataError));
		DataQuery_PrintError();
		return false;
	}
	
	return true;
}

stock bool:DataQuery_RunFast(const String:sQuery[])
{
	new bool:bSuccess = false;
	if(DataQuery_OpenConnection())
	{
		if(DataQuery_Fast(sQuery))
		{
			bSuccess = true;
		}
		
		DataQuery_CloseConnection();
	}
	
	return bSuccess;
}

stock Handle:DataQuery_Simple(const String:sQuery[])
{
	HDataQuery = SQL_Query(HData, sQuery);
	if (HDataQuery == INVALID_HANDLE)
	{
		SQL_GetError(HData, SDataError, sizeof(SDataError));
		DataQuery_PrintError();
	}
	
	return HDataQuery;
}

stock void DataQuery_Close()
{
	if (HDataQuery != INVALID_HANDLE)
	{
		CloseHandle(HDataQuery);
	}
}

stock bool:DataQuery_FetchRow(bool:bStatement = false)
{
	if(bStatement == true)
	{
		if (HDataStatement == INVALID_HANDLE)
		{
			return false;
		}
		
		return SQL_FetchRow(HDataStatement);
	}
	
	if (HDataQuery == INVALID_HANDLE)
	{
		return false;
	}
	
	return SQL_FetchRow(HDataQuery);
}

stock Handle:DataQuery_Prepare(const String:sQuery[])
{
	if (HData == INVALID_HANDLE)
	{
		HDataStatement = INVALID_HANDLE;
		return HDataStatement;
	}

	HDataStatement = SQL_PrepareQuery(HData,sQuery,SDataError,sizeof(SDataError));
	return HDataStatement;
}

// stock bool:DataQuery_EscapeString(const String:sValue[],iMaxLength)
// {
	// new bufferLen = strlen(sValue) * 2 + 1
	// new String:sValue_escaped[bufferLen]
 
	// SQL_EscapeString(HData, sValue, sPlayerName_escaped, bufferLen)
// }

stock void DataQuery_BindParamString(iColumnIndex,const String:sValue[],bool:bCopy)
{
	if (HDataStatement == INVALID_HANDLE)
	{
		return;
	}
	
	SQL_BindParamString(HDataStatement, iColumnIndex, sValue, bCopy);
}

stock void DataQuery_BindParamInt(iColumnIndex,iValue,bool:bSigned)
{
	if (HDataStatement == INVALID_HANDLE)
	{
		return;
	}
	
	SQL_BindParamInt(HDataStatement, iColumnIndex, iValue, bSigned);
}

stock void DataQuery_BindParamFloat(iColumnIndex,Float:fValue)
{
	if (HDataStatement == INVALID_HANDLE)
	{
		return;
	}
	
	SQL_BindParamFloat(HDataStatement, iColumnIndex, fValue);
}

stock bool:DataQuery_Execute()
{
	if(HDataStatement != INVALID_HANDLE && SQL_Execute(HDataStatement))
	{
		return true;
	}
	
	DataQuery_PrintError();
	return false;
}
