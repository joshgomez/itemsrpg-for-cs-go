public Action:Event_PlayerSpawned(Handle:event, const String:name[], bool:dontBroadcast)
{
	new playerID = GetEventInt(event, "userid");
	new client = GetClientOfUserId(playerID);
	
	Game_ResetPlayer(client);
	
	FPlayerChances[ client ][ DamageChance ] = 0.4;
	FPlayerChances[ client ][ ArmorStealChance ] = 0.3;
	FPlayerChances[ client ][ FlashProtectionChance ] = 0.5;
	FPlayerChances[ client ][ FreezeChance ] = 0.7;
	
	FPlayerProperties[ client ][ Damage ] = 2.0;
	FPlayerProperties[ client ][ ArmorSteal ] 	= 0.1;
	FPlayerProperties[ client ][ Gravity ] 	= 0.5;
	
	IPlayerProperties[ client ][ Health ] 	= 20;
	IPlayerProperties[ client ][ Armor ] 	= 20;
	
	Game_UnfrezeePlayer(client);
	
	// new String:ip[16];
	// Utility_GetHostIp(ip,15);
	
	// PrintToChat(client,"%s test",ip);
	
	return Plugin_Continue;
}

public Action:Event_PlayerSpawn(Handle:event, const String:name[], bool:dontBroadcast)
{
	if(CVEnabled.IntValue != 1)
	{
		return Plugin_Continue;
	}

	new playerID = GetEventInt(event, "userid");
	new client = GetClientOfUserId(playerID);
	
	Game_SetHealthOnSpawn(client);
	Game_SetArmorOnSpawn(client);
	Game_SetGravityOnSpawn(client);
	Game_SetSpeedOnSpawn(client);
	
	return Plugin_Continue;
}

public Action:Event_PlayerHurt(Handle:event, const String:name[], bool:dontBroadcast)
{
	if(CVEnabled.IntValue != 1)
	{
		return Plugin_Continue;
	}

	new victimId = GetEventInt(event, "userid");
	new attackerId = GetEventInt(event, "attacker");

	if(victimId == attackerId)
	{
		return Plugin_Continue;
	}

	new victim = GetClientOfUserId(victimId);
	new attacker = GetClientOfUserId(attackerId);
	
	Game_ShakeVictimOnAttack(victim,attacker);
	Game_FreezeVictimOnAttack(victim,attacker);
	
	new dmgOnHealth = GetEventInt(event, "dmg_health");
	Game_LifeStealOnAttack(attacker,dmgOnHealth);
	
	new dmgOnArmor = GetEventInt(event, "dmg_armor");
	Game_ArmorStealOnAttack(attacker,dmgOnArmor);
	
	return Plugin_Continue;
}

public Action:Event_PlayerBlind(Handle:event, const String:name[], bool:dontBroadcast)
{
	if(CVEnabled.IntValue != 1)
	{
		return Plugin_Continue;
	}

	new victimId = GetEventInt(event, "userid");
	new victim = GetClientOfUserId(victimId);
	
	if(Game_FlashProtectionFromFlashBang(victim))
	{
		return Plugin_Handled;
	}
	
	return Plugin_Continue;
}

public Action:Event_PlayerFootstep(Handle:event, const String:name[], bool:dontBroadcast)
{
	//new victimId = GetEventInt(event, "userid");
	//new victim = GetClientOfUserId(victimId);
	

	//return Plugin_Handled;
	
	
	return Plugin_Continue;
}

public Action:Event_PlayerDeath(Handle:event, const String:name[], bool:dontBroadcast)
{
	if(CVEnabled.IntValue != 1)
	{
		return Plugin_Continue;
	}
	
	new victimId = GetEventInt(event, "userid");
	// new attackerId = GetEventInt(event, "attacker");
	
	new victim = GetClientOfUserId(victimId);
	// new attacker = GetClientOfUserId(attackerId);
	
	Game_UnfrezeePlayer(victim);
	
	// new bool:headshot = GetEventBool(event, "headshot");
	
	// decl String:weapon[64];
	// GetEventString(event, "weapon", weapon, sizeof(weapon));

	// decl String:attackerName[64];

	// GetClientName(attacker, attackerName, sizeof(attackerName));
	
	
	return Plugin_Continue;
}

