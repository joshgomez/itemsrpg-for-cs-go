
enum EPlayerPropertiesInt
{
    Health,
    Armor,
	Money,
	HelmetId,
	ArmorId,
	GlovesId,
	ShoesId
}

enum EPlayerPropertiesFloat
{
    Float:LifeSteal,
    Float:ArmorSteal,
	Float:Gravity,
	Float:Speed,
	Float:MagicFind,
	Float:Damage,
	Float:CameraShake
};

enum EPlayerChances
{
	Float:LifeStealChance,
	Float:ArmorStealChance,
	Float:FlashProtectionChance,
	Float:DamageChance,
	Float:CameraShakeChance,
	Float:FreezeChance
};

enum EPlayerCooldowns
{
	Float:LifeStealCooldown,
	Float:ArmorStealCooldown,
	Float:DamageCooldown,
	Float:CameraShakeCooldown,
	Float:FreezeCooldown
};

enum EPlayerStates
{
	bool:Frozen
};

new IPlayerProperties[ MAXPLAYERS + 1 ][ EPlayerPropertiesInt ];
new Float:FPlayerProperties[ MAXPLAYERS + 1 ][ EPlayerPropertiesFloat ];
new Float:FPlayerChances[ MAXPLAYERS + 1 ][ EPlayerChances ];
new Float:FPlayerCooldowns[ MAXPLAYERS + 1 ][ EPlayerCooldowns ];
new bool:BPlayerStates[ MAXPLAYERS + 1 ][ EPlayerStates ];
