
#define FFADE_IN            0x0001
#define FFADE_OUT           0x0002
#define FFADE_MODULATE      0x0004
#define FFADE_STAYOUT       0x0008
#define FFADE_PURGE         0x0010  

stock void Effect_Shake(client, Float:fAmplitude, Float:fDuration)
{
	new Handle:hShake=StartMessageOne("Shake", client, USERMSG_RELIABLE);
	if(hShake!=INVALID_HANDLE)
	{
		PbSetInt(hShake, "command", 0);
		PbSetFloat(hShake, "local_amplitude", fAmplitude);
		PbSetFloat(hShake, "frequency", 1.0);
		PbSetFloat(hShake, "duration", fDuration);
		EndMessage();
	}
}

stock void Effect_Fade(client, Float:fDuration, Float:fHoldTime, flags, const iColors[4])
{
	new iDuration = RoundFloat(fDuration*100.0);
	new iHoldTime = RoundFloat(fHoldTime*100.0);

	new Handle:hFade=StartMessageOne("Fade", client, USERMSG_RELIABLE);
	if(hFade!=INVALID_HANDLE)
	{
		PbSetInt(hFade, "duration", iDuration);
		PbSetInt(hFade, "hold_time", iHoldTime);
		PbSetInt(hFade, "flags", flags);
		PbSetColor(hFade, "clr", iColors);
		EndMessage();
	}
}

stock void Effect_GlowSprite(Float:fVectors[3],ESprites:index,Float:fLive,Float:fDelay,Float:fSize,iBright = 255)
{
	TE_SetupGlowSprite(fVectors, GSprites[ index ],fLive + fDelay, fSize, iBright);
	TE_SendToAll(fDelay);
}