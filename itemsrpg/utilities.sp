
stock bool:Utility_IsPlayer(client)
{
	if(client > 0 && client <= MAXPLAYERS)
	{
		return true;
	}
	
	return false;
}

stock _:Utility_GetHealth(client)
{
	return GetEntProp(client, Prop_Send, "m_iHealth");
}

stock _:Utility_GetArmor(client)
{
	return GetEntProp(client, Prop_Send, "m_ArmorValue");
}

stock _:Utility_GetMaxHealth(client)
{
	return 100 + IPlayerProperties[ client ][ Health ];
}

stock _:Utility_GetMaxArmor(client)
{
	return 100 + IPlayerProperties[ client ][ Armor ];
}

stock void Utility_SetMaxHealth(client,iValue)
{
	SetEntProp(client, Prop_Data, "m_iMaxHealth", iValue);
}

stock void Utility_AddMaxHealth(client,iValue)
{
	new maxHealth = Utility_GetMaxHealth(client);
	Utility_SetMaxHealth(client,maxHealth + iValue);
}

stock void Utility_SetHealth(client,iValue)
{
	SetEntProp(client, Prop_Data, "m_iHealth", iValue);
}

stock void Utility_AddHealth(client,iValue)
{
	new health = Utility_GetHealth(client);
	Utility_SetHealth(client,health + iValue);
}

stock void Utility_SetArmor(client,iValue)
{
	SetEntProp( client, Prop_Data, "m_ArmorValue", iValue);  
}

stock void Utility_AddArmor(client,iValue)
{
	new armor = Utility_GetArmor(client);
	Utility_SetArmor(client,armor + iValue);
}

stock void Utility_SetTranslucent(client, const iColors[4])
{
	SetEntityRenderMode(client, RENDER_TRANSCOLOR);
	SetEntityRenderColor(client, iColors[0],iColors[1],iColors[2],iColors[3]);
}

stock void Utility_SetGravity(client, Float:fValue)
{
	SetEntityGravity(client,fValue);
}

stock void Utility_SetSpeed(client,Float:fValue)
{
	SetEntPropFloat(client, Prop_Send, "m_flLaggedMovementValue",fValue);
}

stock void Utility_MotdTypeUrl(client,String:sTitle[128],String:sUrl[256])
{	
	// new Handle:hMotd = StartMessageOne("VGUIMenu", client);
 
	// PbSetString(hMotd, "name", "info");
	// PbSetBool(hMotd, "show", true);
 
	// new Handle:hSubKey;
 
	// hSubKey = PbAddMessage(hMotd, "subkeys");
	// PbSetString(hSubKey, "name", "type");
	// PbSetString(hSubKey, "str", "2");
 
	// hSubKey = PbAddMessage(hMotd, "subkeys");
	// PbSetString(hSubKey, "name", "title");
	// PbSetString(hSubKey, "str", sTitle);
 
	// hSubKey = PbAddMessage(hMotd, "subkeys");
	// PbSetString(hSubKey, "name", "msg");
	// PbSetString(hSubKey, "str", sUrl);
 
	// EndMessage();
	
	ShowMOTDPanel(client, sTitle, sUrl, MOTDPANEL_TYPE_URL); 
}

stock Utility_GetHostIp(String:sIp[],iMaxLen)
{
	new iHostIP = GetConVarInt(FindConVar("hostip"));
	Format
	(
		sIp, 
		iMaxLen, 
		"%i.%i.%i.%i", 
		(iHostIP >> 24) & 0x000000FF,
		(iHostIP >> 16) & 0x000000FF,
		(iHostIP >> 8) & 0x000000FF,
		(iHostIP) & 0x000000FF
	);
	
	strcopy(sIp, iMaxLen, sIp);
}