
void Game_ResetPlayer(client)
{
	IPlayerProperties[ client ][ Health ] 			= 0;
	IPlayerProperties[ client ][ Armor ] 			= 0;
	IPlayerProperties[ client ][ Money ] 			= 0;
	
	IPlayerProperties[ client ][ HelmetId ] 		= 0;
	IPlayerProperties[ client ][ ArmorId ] 			= 0;
	IPlayerProperties[ client ][ GlovesId ] 		= 0;
	IPlayerProperties[ client ][ ShoesId ] 			= 0;
	
	FPlayerProperties[ client ][ LifeSteal ] 		= 0.0;
	FPlayerProperties[ client ][ ArmorSteal ] 		= 0.0;
	FPlayerProperties[ client ][ Gravity ] 			= 0.0;
	FPlayerProperties[ client ][ Speed ] 			= 0.0;
	FPlayerProperties[ client ][ MagicFind ] 		= 0.0;
	FPlayerProperties[ client ][ Damage ] 			= 0.0;
	FPlayerProperties[ client ][ CameraShake ] 		= 0.0;
	
	FPlayerChances[ client ][ LifeStealChance ] 		= 0.0;
	FPlayerChances[ client ][ ArmorStealChance ] 		= 0.0;
	FPlayerChances[ client ][ FlashProtectionChance ] 	= 0.0;
	FPlayerChances[ client ][ DamageChance ] 			= 0.0;
	FPlayerChances[ client ][ CameraShakeChance ] 		= 0.0;
	FPlayerChances[ client ][ FreezeChance ] 			= 0.0;

	FPlayerCooldowns[ client ][ LifeStealCooldown ] 	= 0.0;
	FPlayerCooldowns[ client ][ ArmorStealCooldown ] 	= 0.0;
	FPlayerCooldowns[ client ][ CameraShakeCooldown ] 	= 0.0;
	FPlayerCooldowns[ client ][ FreezeCooldown ] 		= 0.0;
	FPlayerCooldowns[ client ][ DamageCooldown ] 		= 0.0;
}

bool:Game_IsAbilityCoolingDown(client,EPlayerCooldowns:index,Float:fTimeout)
{
	new Float:fCurrentEngineTime = GetEngineTime();
	if (fCurrentEngineTime - FPlayerCooldowns[ client ][ index ] <= fTimeout) return true;
	FPlayerCooldowns[ client ][ index ] = fCurrentEngineTime;
	
	return false;
}

bool:Game_CanUseAbility(client,FPlayerChances:index)
{
	new Float:fChance = GetRandomFloat(0.0, 1.0);
	if(FPlayerChances[ client ][ index ] >= fChance)
	{
		return true;
	}
	
	return false;
}

bool:Game_ShakeVictimOnAttack(victim,attacker)
{
	new Float:fShakeAmplitude = FPlayerProperties[ attacker ][ CameraShake ];
	if(fShakeAmplitude > 0.0 && Game_CanUseAbility(attacker,FPlayerChances:CameraShakeChance))
	{
		if(!Game_IsAbilityCoolingDown(attacker,EPlayerCooldowns:CameraShakeCooldown,ABILITY_COOLDOWN))
		{
			Effect_Shake(victim,fShakeAmplitude,ABILITY_COOLDOWN);
			return true;
		}
	}
	
	return false;
}

bool:Game_FreezeVictimOnAttack(victim,attacker)
{
	if(Game_CanUseAbility(attacker,FPlayerChances:FreezeChance))
	{
		if(!BPlayerStates[ victim ][ Frozen ] && !Game_IsAbilityCoolingDown(attacker,EPlayerCooldowns:FreezeCooldown,ABILITY_COOLDOWN))
		{
			decl Float:fPosition[3];
			GetClientEyePosition(victim, fPosition);
			EmitAmbientSound(IRPG_SOUND_FREEZE, fPosition, victim, SNDLEVEL_RAIDSIREN);
		
			SetEntityMoveType(victim, MOVETYPE_NONE);
			SetEntityRenderColor(victim, 0, 128, 255, 192);
			
			BPlayerStates[ victim ][ Frozen ] = true;

			CreateTimer(ABILITY_COOLDOWN, Game_UnfrezeeVictimTimer, victim,TIMER_FLAG_NO_MAPCHANGE);
			
			return true;
		}
	}
	
	return false;
}

void Game_UnfrezeePlayer(client)
{
	if(BPlayerStates[ client ][ Frozen ])
	{
		SetEntityRenderColor(client, 255, 255, 255, 255);
		SetEntityMoveType(client, MOVETYPE_WALK);
		BPlayerStates[ client ][ Frozen ] = false;
	}
}

public Action:Game_UnfrezeeVictimTimer(Handle:timer, any:client)
{
	if(IsClientInGame(client) && IsPlayerAlive(client))
	{
		Game_UnfrezeePlayer(client);
	}
	
	return Plugin_Stop;
}

bool:Game_LifeStealOnAttack(attacker,iHealth)
{
	new Float:fLifeSteal = FPlayerProperties[ attacker ][ LifeSteal ];
	if(fLifeSteal > 0.0 && Game_CanUseAbility(attacker,FPlayerChances:LifeStealChance))
	{
		if(!Game_IsAbilityCoolingDown(attacker,EPlayerCooldowns:LifeStealCooldown,ABILITY_COOLDOWN))
		{
			Effect_Fade(attacker,0.6,0.3,FFADE_IN,{200,0,0,50});
			
			new iHealthToSteal = RoundToCeil(float(iHealth)*fLifeSteal);
			new iMaxHealth = Utility_GetMaxHealth(attacker);
			
			if(iHealthToSteal + Utility_GetHealth(attacker) > iMaxHealth)
			{
				Utility_SetHealth(attacker,iMaxHealth);
			}
			else
			{
				Utility_AddHealth(attacker,iHealthToSteal);
			}
			
			return true;
		}
	}
	
	return false;
}

bool:Game_ArmorStealOnAttack(attacker,iArmor)
{
	new Float:fArmorSteal = FPlayerProperties[ attacker ][ ArmorSteal ];
	if(fArmorSteal > 0.0 && Game_CanUseAbility(attacker,FPlayerChances:ArmorStealChance))
	{
		if(!Game_IsAbilityCoolingDown(attacker,EPlayerCooldowns:ArmorStealCooldown,ABILITY_COOLDOWN))
		{
			Effect_Fade(attacker,0.6,0.3,FFADE_IN,{0,0,200,50});
			
			new iArmorToSteal = RoundToCeil(float(iArmor)*fArmorSteal);
			new iMaxArmor = Utility_GetMaxArmor(attacker);
			
			if(iArmorToSteal + Utility_GetArmor(attacker) > iMaxArmor)
			{
				Utility_SetArmor(attacker,iMaxArmor);
			}
			else
			{
				Utility_AddArmor(attacker,iArmorToSteal);
			}
			
			return true;
		}
	}
	
	return false;
}

bool:Game_FlashProtectionFromFlashBang(victim)
{
	if(IsPlayerAlive(victim))
	{
		if(Game_CanUseAbility(victim,FPlayerChances:FlashProtectionChance))
		{
			SetEntDataFloat(victim,FindSendPropOffs("CCSPlayer", "m_flFlashMaxAlpha"),0.0);
			return true;
		}
		
		new Float:fMaxAlpha = GetEntDataFloat(victim, FindSendPropOffs("CCSPlayer", "m_flFlashMaxAlpha"));
		if(fMaxAlpha == 0.0)
		{
			SetEntDataFloat(victim,FindSendPropOffs("CCSPlayer", "m_flFlashMaxAlpha"),255.0);
		}
	}
	
	return false;
}

Float:Game_ExtraDamageOnAttack(victim,attacker)
{
	new Float:fDamage = FPlayerProperties[ attacker ][ Damage ];
	fDamage = GetRandomFloat(0.0,fDamage);
	if(fDamage > 0.0 && Game_CanUseAbility(attacker,FPlayerChances:DamageChance))
	{
		if(!Game_IsAbilityCoolingDown(attacker,EPlayerCooldowns:DamageCooldown,ABILITY_COOLDOWN))
		{
			new Float:fPosition[3];
			GetEntPropVector(victim, Prop_Send, "m_vecOrigin", fPosition);
			Effect_GlowSprite(fPosition,ESprites:PurpleGlow,1.0,0.1,0.6);
			
			return fDamage;
		}
	}
	
	return 0.0;
}

void Game_SetHealthOnSpawn(client)
{
	Utility_AddMaxHealth(client,IPlayerProperties[ client ][ Health ]);
	Utility_AddHealth(client,IPlayerProperties[ client ][ Health ]);
}

void Game_SetArmorOnSpawn(client)
{
	Utility_AddArmor(client,IPlayerProperties[ client ][ Armor ]);
}

void Game_SetGravityOnSpawn(client)
{
	Utility_SetGravity(client,1.0 - FPlayerProperties[ client ][ Gravity ]);
}

void Game_SetSpeedOnSpawn(client)
{
	Utility_SetSpeed(client,1.0 + FPlayerProperties[ client ][ Speed ]);
}
