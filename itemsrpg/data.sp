
stock Data_Up_CreatePlayerTable()
{
	DataQuery_RunFast("\
	CREATE TABLE IF NOT EXISTS `Players`\
	(\
		`Id` INT unsigned NOT NULL AUTO_INCREMENT,\
		`AuthID` VARCHAR(20) NOT NULL,\
		`PlayerName` VARCHAR(64) DEFAULT NULL,\
		`HelmetId` INT unsigned DEFAULT NULL,\
		`ArmorId` INT unsigned DEFAULT NULL,\
		`GlovesId` INT unsigned DEFAULT NULL,\
		`ShoesId` INT unsigned DEFAULT NULL,\
		`Money` INT unsigned DEFAULT 0,\
		UNIQUE (`authID`),\
		PRIMARY KEY(`Id`)\
	)");
}

stock Data_Down_CreatePlayerTable()
{
	DataQuery_RunFast("DROP TABLE IF EXISTS `Players`");
}

stock Data_Up_CreateInventoryTable()
{
	DataQuery_RunFast("\
	CREATE TABLE IF NOT EXISTS `Inventories`\
	(\
		`Id` INT unsigned NOT NULL AUTO_INCREMENT,\
		`PlayerId` INT unsigned NOT NULL,\
		`ItemId` INT unsigned NOT NULL,\
		PRIMARY KEY(`Id`)\
	)");
}

stock Data_Down_CreateInventoryTable()
{
	DataQuery_RunFast("DROP TABLE IF EXISTS `Inventories`");
}

stock Data_Up_CreateItemTable()
{
	DataQuery_RunFast("\
	CREATE TABLE IF NOT EXISTS `Items`\
	(\
		`Id` INT unsigned NOT NULL AUTO_INCREMENT,\
		`Type` ENUM('Other','Helmet','Armor','Gloves','Shoes'),\
		`Name` VARCHAR(64) DEFAULT NULL,\
		`Rarity` INT unsigned DEFAULT 0,\
		`Health` TINYINT unsigned DEFAULT 0,\
		`Armor` TINYINT unsigned DEFAULT 0,\
		`LifeSteal` FLOAT DEFAULT 0,\
		`ArmorSteal` FLOAT DEFAULT 0,\
		`Gravity` FLOAT DEFAULT 0,\
		`Speed` FLOAT DEFAULT 0,\
		`MagicFind` FLOAT DEFAULT 0,\
		`Damage` FLOAT DEFAULT 0,\
		`CameraShake` FLOAT DEFAULT 0,\
		`LifeStealChance` FLOAT DEFAULT 0,\
		`ArmorStealChance` FLOAT DEFAULT 0,\
		`FlashProtectionChance` FLOAT DEFAULT 0,\
		`DamageChance` FLOAT DEFAULT 0,\
		`FreezeChance` FLOAT DEFAULT 0,\
		`Purchasable` ENUM('No','Yes'),\
		UNIQUE (`Name`),\
		PRIMARY KEY(`Id`)\
	)");
}

stock Data_Down_CreateItemTable()
{
	DataQuery_RunFast("DROP TABLE IF EXISTS `Items`");
}

void Data_InsertItemPack1()
{
	DataQuery_RunFast("\
	INSERT IGNORE INTO `Items` (`Type`,`Name`,`Rarity`,`Health`,`Armor`,`Purchasable`)\
	VALUES\
		('Helmet'	,'Leather Helmet'	,1 	,0 	,2 	,'Yes'),\
		('Armor'	,'Leather Armor'	,1 	,1 	,3 	,'Yes'),\
		('Gloves' 	,'Leather Gloves'	,1 	,0 	,1 	,'Yes'),\
		('Shoes' 	,'Leather Shoes'	,1 	,0 	,1 	,'Yes')\
	");
}

void Data_Init()
{
	Data_Down_CreatePlayerTable();
	Data_Down_CreateInventoryTable();
	Data_Down_CreateItemTable();

	Data_Up_CreatePlayerTable();
	Data_Up_CreateInventoryTable();
	Data_Up_CreateItemTable();
	
	Data_InsertItemPack1();
}

void Data_InsertPlayer(const String:sAuthId[],const String:sPlayerName[])
{
	if(DataQuery_OpenConnection())
	{
		DataQuery_Prepare("SELECT * FROM `Players` WHERE `AuthId` = ? LIMIT 1");
		DataQuery_BindParamString(0,sAuthId,false);
		DataQuery_Execute();
		
		if(!DataQuery_FetchRow(true))
		{
			DataQuery_Prepare("INSERT INTO `Players`(`AuthId`,`PlayerName`) VALUES(?,?)");
			DataQuery_BindParamString(0,sAuthId,false);
			DataQuery_BindParamString(1,sPlayerName,false);
			DataQuery_Execute();
		}
		
		DataQuery_CloseConnection();
	}
}