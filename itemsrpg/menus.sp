
public Menu_ItemsRPGHandler(Handle:hMenu, MenuAction:mAction, client, menuItem)
{
	if (mAction == MenuAction_Select)
	{
		new String:sInfo[32];
		new bool:bFound = GetMenuItem(hMenu, menuItem, sInfo, sizeof(sInfo));
		
		PrintToConsole(client, "You selected item: %d (found? %d info: %s)", menuItem, bFound, sInfo);
	}
	else if (mAction == MenuAction_Cancel)
	{
		PrintToServer("Client %d's menu was cancelled.  Reason: %d", client, menuItem);
	}
	else if (mAction == MenuAction_End)
	{
		CloseHandle(hMenu);
	}
}
 
public Action:Menu_CommandItemsRPG(client, args)
{
	new Handle:hMenu = CreateMenu(Menu_ItemsRPGHandler);
	SetMenuTitle(hMenu, "ItemsRPG Menu");
	AddMenuItem(hMenu, "inventory"	,"Inventory");
	//AddMenuItem(hMenu, "items"		,"Items");
	AddMenuItem(hMenu, "equipment"	,"Equipment");
	SetMenuExitButton(hMenu, true);
	DisplayMenu(hMenu, client, 300);
 
	return Plugin_Handled;
}